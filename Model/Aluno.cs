using System.ComponentModel.DataAnnotations;

public class Aluno {
    public int Matricula { get; set; }
    public string nome { get; set; }
    [Key]
    public string Cpf { get; set; }
    public string Curso { get; set; }
}