using System.ComponentModel.DataAnnotations;

public class Livro {
    [Key]
    public int Id { get; set; }
    public string Titulo { get; set; }
    public string Autor { get; set; }
    public int anoPublicacao { get; set; }
    public string Edicao { get; set; }
    public string Editora { get; set; }
}