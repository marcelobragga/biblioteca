using Microsoft.EntityFrameworkCore;

public class Context : DbContext{
    public Context(DbContextOptions <Context> contexto) : base(contexto)
    {
        
    }
    
     public DbSet<Aluno> Aluno { get; set; }
     public DbSet<Gerente> Gerente { get; set; }
     public DbSet<Livro> Livro { get; set; }
     public DbSet<Locacao> Locacao { get; set; }
     public DbSet<Revista> Revista { get; set; }

}