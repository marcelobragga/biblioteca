import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Container } from 'react-bootstrap';
import { Home } from './components/Home';


function App() {
  return (
    <Container>
      <BrowserRouter>
  <Navbar bg='light' expand='lg'>
    <Navbar.Brand as={Link} to="/">Nome da Minha Aplicação</Navbar.Brand>
    <Navbar.Toggle aria-controls='basic-navbar-nav' />
    <Navbar.Collapse id='basic-navbar-nav'>
      <Nav className='mr-auto'>
        <Nav.Link as={Link} to="/">Home</Nav.Link>
        <NavDropdown title='Cadastros' id='basic-nav-dropdown'>
          <NavDropdown.Item as={Link} to='/tarefas'>Tarefas</NavDropdown.Item>
        </NavDropdown>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  <Switch>
  </Switch>
</BrowserRouter>

    </Container>
  );
}

export default App;
